'''
@Author Akash Roy <skyrocker>
'''

import math

def insideCircle(x, y):
    eq = math.sqrt(((50.0 - x) ** 2) + ((50.0 - y) ** 2))
    return True if eq <= 50 else False


def angleToPoint(x, y):
    angle = 90.0
    if y != 50.0:
        slope = (50.0 - x)/(50.0 - y)
        angle = math.degrees(math.atan(slope))
        if angle < 0 or y == 0:
            angle = angle + 180.0
    if x < 50:
        angle = angle + 180.0
    return angle
            

def shadedAngle(p):
    return p * 360.0 / 100.0


t = int(raw_input())
xc, yc = 50.0, 50.0
pre, shadAngle = 0.0, 0.0
for i in range(t):
    p, x, y = map(float, raw_input().split(' '))
    if insideCircle(x, y) :
        if pre != p:
            pre = p
            shadAngle = shadedAngle(p)
        anglePoint = angleToPoint(x, y)
        rel = anglePoint - shadAngle
        if rel < 0:
            print "Case #"+str(i+1)+": black"
        else:
            print "Case #"+str(i+1)+": white"
    else:
        print "Case #"+str(i+1)+": white"
        
