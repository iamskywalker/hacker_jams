'''
@Author Akash Roy <skyrocker>
'''
t = int(raw_input())
for _ in range(t):
    n, m = map(int, raw_input().strip().split(" "))
    l = []
    items = [ 0 for _ in range(n)]
    iniSum = 0
    sqSum = 0
    totSum = 0
    for d in range(n):
        mm = sorted(map(int, raw_input().strip().split(" ")))
        count = 0
        if len(l) == 0:
            l = list(mm)
            iniSum = sum(l)
            sqSum = len(mm) ** 2
            totSum = iniSum + sqSum
            items[d] = len(mm)
        elif len(l) < n :
            for i in range(len(mm)):
                if len(l) < n:
                    l.append(mm[i])
                    count = count + 1
            iniSum = sum(l)
            sqSum = sqSum + count ** 2
            items[d] = count
            totSum = iniSum + sqSum
            l=l[:d] + sorted(l[d:])
            print "total cost : ",totSum,"itemList :",items
        else :
            i=0
            c=m-d
            #print "c : ",c,"m :",m
            while c > 0 and i < m:
                if l[m-c] > mm[i] and items[d-1] > -1:
                    l[m-c] = mm[i]
                    count = count + 1
                    c = c - 1
                    i = i + 1
                    iniSum = sum(l)
                    items[d-1] = items[d-1] - 1
                    items[d] = count
                    totSum = iniSum + sum(i ** 2 for i in items)
                elif l[m-c] == mm[i]:
                    c = c - 1
                else :
                    i = i + 1
                l=l[:d] + sorted(l[d:])
                print "c : ",c,"m :",m,"i:",i
        #print "d = ",d,"l = >",l
    print "total cost : ",totSum,"itemList :",items
    print l
