def input_chunker(m):
    astr = "".join(input().split())
    return [astr[i:i+m] for i in range(0, len(astr), m)]

T = int(input())
for t in range(T):
    inputs = input().strip().split()
    n = int(inputs[0])
    k = int(inputs[1])
    a = input_chunker(k)
    b = input_chunker(k)
    nChunk = len(b)
    isPossible = True
    x = a.index(b[0])
    if x == -1:
        isPossible = False
    elif k == 0:
        isPossible = a == b
    else:
        for i in range(n):
            if a[x] != b[i]:
                isPossible = False
                break
            x = (x+1)%n if x == n-1 else x+1
    print("Case #{}: {}".format(t+1,"YES" if isPossible else "NO"))
