T = int(raw_input())
for t in range(T):
	inputs = raw_input().strip().split(" ")
	n = int(inputs[0])
	k = int(inputs[1])
	v = long(inputs[2])
	places = [ raw_input() for _ in range(n)]
	noOfIter = v % n
	if n == k:
		print "Case #"+str(t+1)+": "+" ".join(places)
	elif n > k and noOfIter == 0:
		start = len(places) - k
		end = len(places)
		print "Case #"+str(t+1)+": "+" ".join(places[start : end])
	else:
		l = []
		j = 0
		index = 0
		for i in range(noOfIter):
			z = k
			while z > 0:
				if i == noOfIter - 1 :
					l.insert(index, places[j])
				z = z - 1
				j = 0 if j + 1 == n else j + 1
				index = 0 if j == 0 else index + 1
		print "Case #"+str(t+1)+": "+" ".join(l)

