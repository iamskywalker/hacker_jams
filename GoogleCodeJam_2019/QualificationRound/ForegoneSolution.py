#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  6 22:01:43 2019

@author: akash
"""

t=int(raw_input().strip())
for k in range(t):
    n = int(raw_input())
    first = second = 0
    if n % 2 == 0:
        first = second = n/2
    else:
        first = n/2
        second = first + 1
    firstList = list(str(first))
    secondList = list(str(second))
    for i in range(len(firstList)):
        if firstList[i] == '4' or secondList[i] == '4':
            if firstList[i] > secondList[i]:
                firstList[i] = str(int(firstList[i]) + 1)
                secondList[i] = str(int(secondList[i]) - 1)
            else:
                firstList[i] = str(int(firstList[i]) - 1)
                secondList[i] = str(int(secondList[i]) + 1)
    print "Case #{}: {} {}".format(k + 1, ''.join(firstList), ''.join(secondList))
