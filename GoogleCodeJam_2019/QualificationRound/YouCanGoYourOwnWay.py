#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  6 21:40:17 2019

@author: akash
"""
d = dict()
d["E"]="S"
d["S"]="E"
t=int(raw_input().strip())
for k in range(t):
    n = int(raw_input())
    line = raw_input()
    result  = list(line)
    for i in range(len(result)):
        result[i] = d[result[i]]
    print "Case #" + str(k+1) + ":", ''.join(result)
